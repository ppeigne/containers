import io.circe.Decoder.Result
import io.circe.{Decoder, Encoder, HCursor, Json}
import io.circe.parser._
import io.circe.generic.semiauto._
import io.circe.generic.JsonCodec
import io.circe.syntax._

@JsonCodec case class HistoryEntry(
                                    created:     String,//DockerDate,
                         created_by:  String,
                         empty_layer: Option[Boolean],
                         author:      Option[String],
                         comment:     Option[String]
                       )

implicit val HistoryEntryDecoder: Decoder[HistoryEntry] = new Decoder[HistoryEntry] {
  final def apply(cursor: HCursor): Decoder.Result[HistoryEntry] = {
    val created = cursor.get[String]("created").getOrElse("")
    val created_by = cursor.downField("container_config").downField("Cmd").as[List[String]].getOrElse(List("")).head
    val empty_layer = Some(cursor.get[Boolean]("throwaway").getOrElse(false))
    val author = Some(cursor.get[String]("author").getOrElse(""))
    val comment = Some(cursor.get[String]("comment").getOrElse(""))
    new Right(HistoryEntry(created, created_by, empty_layer, author, comment))
  }
}

implicit val historyEntryEncoder: Encoder[HistoryEntry] = deriveEncoder /*new Encoder[HistoryEntry] {
  override def apply(a: HistoryEntry): Json = Json.obj(
    ("created", Json.fromString(a.created)),
    ("created_by", Json.fromString(a.created_by)),
    ("empty_layer", Json.fromBoolean(a.empty_layer.get)),
    ("author", Json.fromString(a.author.get)),
    ("comment", Json.fromString(a.comment.get))
  )
}
*/

val test2 = "{\"id\":\"67ed2c162d9e08533291c4de0caea1d60b0bed90f0b4e2fec4076149d3705b84\",\"parent\":\"c15b69c7b0e6b83540dff04f6481fee5bf7d67087cfe651735fdab9fd8885be7\",\"created\":\"2019-02-06T12:02:34.387238501Z\",\"container_config\":{\"Cmd\":[\"/bin/sh -c #(nop)  ENV PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin\"]},\"throwaway\":true}"
val dec2 = parse(test2)

val x = decode[HistoryEntry](test2) match {
  case Right(value) => historyEntryEncoder(value)
  case _ => Json.Null
}

x