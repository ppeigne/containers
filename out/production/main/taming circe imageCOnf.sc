import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import Container.{ContainerImage, DockerMetadata}
import Container.ImageDownloader.getConfigAsString
import Container.Registry.{downloadManifest, manifest}
import Container.Status.note
import io.circe.generic.semiauto._
import io.circe.parser._
import io.circe.syntax._
import io.circe.{Decoder, Encoder, HCursor, Json}
import squants.time.Seconds

/*
case class EmptyObject()
implicit val EmptyObjectDecoder: Decoder[EmptyObject] = deriveDecoder
implicit val EmptyObjectEncoder: Encoder[EmptyObject] = deriveEncoder
*/
/*
type Volumes = Map[String, EmptyObject]
implicit val VolumesDecoder: Decoder[Volumes] = Decoder.decodeMap[String, EmptyObject]
implicit val VolumesEncoder: Encoder[Volumes] = Encoder.encodeMap[String, EmptyObject]
*/
/*
case class HealthCheck(
                        Test:     Option[List[String]],
                        Interval: Option[Int],
                        Timeout:  Option[Int],
                        Retries:  Option[Int]
                      )
implicit val HealthCheckDecoder: Decoder[HealthCheck] = deriveDecoder
implicit val HealthCheckEncoder: Encoder[HealthCheck] = deriveEncoder
*/
/*
case class HistoryEntry(
                                    created:     String,//DockerDate,
                                    created_by:  String,
                                    empty_layer: Option[Boolean],
                                    author:      Option[String],
                                    comment:     Option[String]
                                  )

implicit val HistoryEntryDecoder: Decoder[HistoryEntry] = new Decoder[HistoryEntry] {
  final def apply(cursor: HCursor): Decoder.Result[HistoryEntry] = {
    val created = cursor.get[String]("created").getOrElse("")
    val created_by = cursor.downField("container_config").downField("Cmd").as[List[String]].getOrElse(List("")).head
    val empty_layer = Some(cursor.get[Boolean]("throwaway").getOrElse(false))
    val author = Some(cursor.get[String]("author").getOrElse(""))
    val comment = Some(cursor.get[String]("comment").getOrElse(""))
    new Right(HistoryEntry(created, created_by, empty_layer, author, comment))
  }
}
implicit val historyEntryEncoder: Encoder[HistoryEntry] = deriveEncoder
*/

 case class ContainerConfig(
                            // <--- documented fields  --->
                            User:         Option[String]              = Some(""),
                            Memory:       Option[Int]                 = Some(0),
                            MemorySwap:   Option[Int]                 = Some(0),
                            CpuShares:    Option[Int]                 = Some(0),
                            ExposedPorts: Option[Map[String, EmptyObject]] = None,
                            //    ExposedPorts: Ports               = Map.empty,

                            Env:          Option[List[String]]        = Some(List.empty),
                            Entrypoint:   Option[List[String]]        = Some(List.empty),
                            Cmd:          Option[List[String]]        = Some(List.empty),
                            Healthcheck:  Option[HealthCheck] = None,

                            Volumes:      Option[Volumes]             = Some(Map.empty),

                            WorkingDir:   Option[String]              = Some(""),
                            // <--- extra fields not part of the spec: implementation specific --->
                            Domainname:   Option[String]                   = Some(""), // udocker specific
                            AttachStdout: Option[Boolean]                  = None,
                            Hostname:     Option[String]                   = Some(""), // udocker specific
                            StdinOnce:    Option[Boolean]                  = None,
                            Labels:       Option[Map[String, EmptyObject]] = None, // FIXME what is this?
                            AttachStderr: Option[Boolean]                  = None,
                            OnBuild:      Option[List[String]]             = None, // FIXME what is this?
                            Tty:          Option[Boolean]                  = None,
                            OpenStdin:    Option[Boolean]                  = None,
                            Image:        Option[String]                   = None,
                            AttachStdin:  Option[Boolean]                  = None,
                            ArgsEscaped:  Option[Boolean]                  = None
                          )
implicit val ContainerConfigDecoder: Decoder[ContainerConfig] = deriveDecoder
implicit val ContainerConfigEncoder: Encoder[ContainerConfig] = deriveEncoder

///////////////


case class RootFS(
                   `type`:   String       = "layers",
                   diff_ids: List[String] = List.empty
                 )
implicit val RootFSDecoder: Decoder[RootFS] = deriveDecoder
implicit val RootFSEncoder: Encoder[RootFS] = deriveEncoder


type ContainerID = String

type DockerDate = LocalDateTime

val dockerDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSSS'Z'")
implicit val encodeDate: Encoder[DockerDate] = Encoder.encodeString.contramap[DockerDate](dockerDateFormatter.format(_))

implicit val decodeDate: Decoder[DockerDate] = Decoder.decodeString.emap { str ⇒
  try Right(LocalDateTime.parse(str, dockerDateFormatter))
  catch {
    case e: Exception ⇒ Left("Date")
  }
}


case class ImageJSON(
                      // <--- documented fields  --->
                      created:      Option[DockerDate] = None,
                      author:       Option[String]             = Some(""),
                      architecture: Option[String]             = Some(""),
                      os:           Option[String]             = Some(""),

                      //config:       List[String] = List.empty,
                      config:       Option[ContainerConfig]    = Some(ContainerConfig()),
                      rootfs:       Option[RootFS]             = Some(RootFS()),
                      history:      Option[List[HistoryEntry]] = Some(List.empty),
                      // <--- extra fields not part of the spec: implementation specific --->
                      id:               Option[String],
                      parent:           Option[String],
                      docker_version:   Option[String]          = None,
                      container:        Option[ContainerID]     = None,
                      container_config: Option[ContainerConfig] = None
                    )

implicit val imageJSONDecoder: Decoder[ImageJSON] = deriveDecoder
/*
import Container.Registry.Manifest
def v1HistoryToImageJson(manifest: Manifest): ImageJSON = {
  val rawJsonImage = parse(manifest.value.history.get.head.v1Compatibility).getOrElse(Json.Null)
  val cursor: HCursor = rawJsonImage.hcursor

  val created = cursor.get[DockerDate]("created").toOption
  val author = cursor.get[String]("author").toOption
  val architecture = cursor.get[String]("architecture").toOption
  val os = cursor.get[String]("os").toOption
  val config = cursor.downField("config").as[ContainerConfig].toOption //
  val rootfs = cursor.get[RootFS]("rootfs").toOption
  val history =
    for ( x <- manifest.value.history.get.tail.reverse) yield decode(x.v1Compatibility)//)
  val id = cursor.get[String]("id").toOption
  val parent = cursor.get[String]("parent").toOption
  val docker_version = cursor.get[String]("docker_version").toOption
  val container = cursor.get[String]("container").toOption
  val container_config = cursor.get[ContainerConfig]("container_config").toOption
  ImageJSON(created, author,architecture, os, config, rootfs, history, id, parent, docker_version,
            container, container_config)
}
*/

//////////////


val test = "{\"architecture\":\"amd64\",\"config\":{\"Hostname\":\"\",\"Domainname\":\"\",\"User\":\"\",\"AttachStdin\":false,\"AttachStdout\":false,\"AttachStderr\":false,\"Tty\":false,\"OpenStdin\":false,\"StdinOnce\":false,\"Env\":[\"PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin\",\"LANG=C.UTF-8\",\"GPG_KEY=0D96DF4D4110E5C43FBFB17F2D347EA6AA65421D\",\"PYTHON_VERSION=3.6.8\",\"PYTHON_PIP_VERSION=19.0.2\"],\"Cmd\":[\"python3\"],\"ArgsEscaped\":true,\"Image\":\"sha256:b4146f74f254b035dc14f507dcec3f64cf0e09c8d0476de9e692275da660dc84\",\"Volumes\":null,\"WorkingDir\":\"\",\"Entrypoint\":null,\"OnBuild\":null,\"Labels\":null},\"container\":\"a38d97f781f1b56394dc0c90bddab535137f71e6945426c0c93c0190c46ffabb\",\"container_config\":{\"Hostname\":\"a38d97f781f1\",\"Domainname\":\"\",\"User\":\"\",\"AttachStdin\":false,\"AttachStdout\":false,\"AttachStderr\":false,\"Tty\":false,\"OpenStdin\":false,\"StdinOnce\":false,\"Env\":[\"PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin\",\"LANG=C.UTF-8\",\"GPG_KEY=0D96DF4D4110E5C43FBFB17F2D347EA6AA65421D\",\"PYTHON_VERSION=3.6.8\",\"PYTHON_PIP_VERSION=19.0.2\"],\"Cmd\":[\"/bin/sh\",\"-c\",\"#(nop) \",\"CMD [\\\"python3\\\"]\"],\"ArgsEscaped\":true,\"Image\":\"sha256:b4146f74f254b035dc14f507dcec3f64cf0e09c8d0476de9e692275da660dc84\",\"Volumes\":null,\"WorkingDir\":\"\",\"Entrypoint\":null,\"OnBuild\":null,\"Labels\":{}},\"created\":\"2019-02-12T21:37:33.563084814Z\",\"docker_version\":\"18.06.1-ce\",\"id\":\"8bc0297f20768ebb7f753b5c9686fe425848d50bce9152f5e02354e6a5470c02\",\"os\":\"linux\",\"parent\":\"388086fb09673a409174363a7c81cf44430a1eed24d604b958e78ebb2c1f7149\",\"throwaway\":true}"
val json = parse(test).getOrElse(Json.Null)

val cursor: HCursor = json.hcursor

val config = cursor.downField("config").as[ContainerConfig]

println(config)

config.getOrElse(ContainerConfig()).asJson

/*
val test2 = "{\"id\":\"67ed2c162d9e08533291c4de0caea1d60b0bed90f0b4e2fec4076149d3705b84\",\"parent\":\"c15b69c7b0e6b83540dff04f6481fee5bf7d67087cfe651735fdab9fd8885be7\",\"created\":\"2019-02-06T12:02:34.387238501Z\",\"container_config\":{\"Cmd\":[\"/bin/sh -c #(nop)  ENV PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin\"]},\"throwaway\":true}"
val dec2 = parse(test2)

val x = decode[HistoryEntry](test2) match {
  case Right(value) => historyEntryEncoder(value)
  case _ => Json.Null
}

x*/

case class DockerImage(imageName: String,
                       tag: String = "latest",
                       registry: String = "https://registry-1.docker.io",
                       command: Seq[String] = Seq())

val img = DockerImage("python","3.6-stretch")

manifest(img, downloadManifest(dockerImage, Seconds(10))(net)) match {
  case Right(value) => {
    // 1. First get the configData
    note(" - collecting configData")

  }
}