package Container

import org.scalatest._
import scala.sys.process._

abstract class UnitTest (component: String) extends FlatSpec with Matchers
/*
class LoaderTest extends UnitTest("Loader") {

  val dockerLs = Seq("docker", "run", "-i", "python:3.6-stretch", "/bin/ls").!!
  val dockerLsBin = Seq("docker", "run", "-i", "python:3.6-stretch", "/bin/ls", "/bin").!!
  val pRootLs = Seq("proot", "-r", "/home/pierre/Desktop/containers/python/rootfs", "/bin/ls").!!
  val pRootLsBin = Seq("proot", "-r", "/home/pierre/Desktop/containers/python/rootfs", "/bin/ls", "/bin").!!
  val scriptLs = Seq("./python/launcher.sh", "run", "/usr/bin/proot", "/home/pierre/Desktop/containers/python/rootfs", "/bin/ls").!!
  val scriptLsBin = Seq("./python/launcher.sh", "run", "/usr/bin/proot", "/home/pierre/Desktop/containers/python/rootfs", "/bin/ls", "/bin").!!


  "pRoot" should "build same / dir as Docker with the loader image" in {
    assert(dockerLs == pRootLs)
  }
  "pRoot" should "build same /bin dir as Docker with the loader image" in {
    assert(dockerLsBin == pRootLsBin)
  }

  "loader script" should "build same / dir as Docker with the loader image" in {
    assert(dockerLs == scriptLs)
  }
  "loader script" should "build same /bin dir as Docker with the loader image" in {
    assert(dockerLsBin == scriptLsBin)
  }

  "loader script" should "build same / dir as pRoot with the loader image" in {
    assert(pRootLs == scriptLs)
  }
  "loader script" should "build same /bin dir as pRoot with the loader image" in {
    assert(pRootLsBin == scriptLsBin)
  }
}
*/
class BuilderTest extends UnitTest("Builder") {
  def compareRootFS(imageName: String) = {
    val run = Seq("sbt", "run", imageName)
    val pRootPath = Seq("/home/pierre/Desktop/proot-oci-loader")
  }


  val dockerImageRootFs = (Seq("find", "/home/pierre/Desktop/containers/python/rootfs/usr", "-maxdepth", "5") #| Seq("cut", "-d/", "-f8-")).!!
  val containerImageRootFs = (Seq("find", "/home/pierre/Desktop/proot-oci-loader/python/rootfs/usr", "-maxdepth", "5") #| Seq("cut", "-d/", "-f8-")).!!
  "OCI LOADER" should "extract and build the same /rootfs dir as with Docker image" in {
    assert(dockerImageRootFs == containerImageRootFs)
  }
/*
  "OCI LOADER" should "extract and build the same /rootfs/bin dir as with Docker image" in {
      assert((dockerImageRootFs :+ "/bin").!! == (containerImageRootFs :+ "/bin").!!)

  }

  "OCI LOADER" should "extract and build the same /rootfs/boot dir as with Docker image" in {
      assert((dockerImageRootFs :+ "/boot").!! == (containerImageRootFs :+ "/boot").!!)

  }
  "OCI LOADER" should "extract and build the same /rootfs/dev dir as with Docker image" in {
      assert((dockerImageRootFs :+ "/dev").!! == (containerImageRootFs :+ "/dev").!!)
  }
*/

}

/*
  val dockerPython = Seq("docker", "run", "-i", "python:3.6-stretch", "find", "-name", "python").!!
  val pRootPython = Seq("proot", "-r",  "/home/pierre/Desktop/containers/python/rootfs", "find", "-name", "python").!!
  val scriptPython = Seq("./python/launcher.sh", "run", "/usr/bin/proot", "/home/pierre/Desktop/containers/python/rootfs","find", "-name", "python").!!


  "pRoot" should "build same python path as Docker with the loader image" in {
    assert(pRootPython == dockerPython)
  }

  "loader script" should "build same python path as Docker with the loader image" in {
    assert(scriptPython == dockerPython)
  }

  "loader script" should "build same python path as pRoot with the loader image" in {
    assert(scriptPython == pRootPython)
  }
*/
